#!/usr/bin/bash

#######################################################################
#
# ALTERAR UM APP DEFAULT EM UM SERVIDOR NGINX COM PHP-7.2
#
#######################################################################

DIR_NGINX_SITES='/etc/nginx/sites-available'

function change-app {
	if [ -s "$DIR_NGINX_SITES/$1" ]
	then
		echo
		echo "Alterando para '$1'"
		ln -sfn "$DIR_NGINX_SITES/$1" "$DIR_NGINX_SITES/default"
		
		echo 'Reiniciando servicos...'
		service nginx restart
		service php7.2-fpm restart
		echo 'Fim.'
	else
		echo "'$1' nao eh um app valido"
		return 1
	fi
}

if [ -z $1 ] 
then
	echo 'Escolha um App:'
	ls $DIR_NGINX_SITES
	read app
	change-app $app
else
	change-app $1
fi
