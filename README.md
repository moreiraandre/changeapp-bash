# Change WebApp in server NGINX

### Download script
```sh
sudo wget -O /usr/local/bin/changeapp 'https://gitlab.com/api/v4/projects/19764666/repository/files/changeapp.sh/raw?ref=master' && sudo chmod +x /usr/local/bin/changeapp
```

### Execute script
```sh
changeapp

# Or
changeapp <app-name>
```
